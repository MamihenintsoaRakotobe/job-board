import axios from "./client";
import qs from "qs";
import { jobReducer } from "./utils";

const apiURL = process.env.STRAPI_API_BASE_URL;

export const getJobs = async ({ page = 1, pageSize = 10 } = {}) => {
  const query = qs.stringify(
    {
      pagination: {
        page,
        pageSize,
      },
      populate: ["company", "company.logo", "company.coverImage", "skillsTags"],
    },
    {
      encodeValuesOnly: true,
    }
  );
  const res = await axios.get(`${apiURL}/jobs?${query}`);
  const rawJobs = res.data.data;
  const jobs = rawJobs.map((rawJob) => jobReducer(rawJob, false));
  return jobs;
};

export const getJobsSlugs = async () => {
  const query = qs.stringify(
    {
      fields: ["slug"],
    },
    {
      encodeValuesOnly: true,
    }
  );

  const res = await axios.get(`${apiURL}/jobs?${query}`);
  const rawSlugs = res.data.data;
  const slugs = rawSlugs.map((rawSlug) => rawSlug.attributes.slug);
  return slugs;
};

export const getJobBySlug = async ({ slug }) => {
  const query = qs.stringify(
    {
      filters: {
        slug: {
          $eq: slug,
        },
      },
      populate: [
        "company",
        "company.logo",
        "company.coverImage",
        "relatedJobs",
        "relatedJobs.company",
        "relatedJobs.company.logo",
        "relatedJobs.company.coverImage",
        "skillsTags",
      ],
    },
    {
      encodeValuesOnly: true,
    }
  );
  const res = await axios.get(`${apiURL}/jobs?${query}`);

  const rawJob = res.data.data[0];
  return jobReducer(rawJob);
};

export const getJobsByCompanyId = async ({ id }) => {
  const query = qs.stringify(
    {
      filters: {
        company: {
          id: {
            $eq: id,
          },
        },
      },
      populate: ["company", "company.logo", "company.coverImage", "skillsTags"],
    },
    {
      encodeValuesOnly: true,
    }
  );

  const res = await axios.get(`${apiURL}/jobs?${query}`);

  const rawJobs = res.data.data;

  const jobs = rawJobs.map((rawJob) => jobReducer(rawJob, false));

  return jobs;
};

export const searchJobs = async (query) => {
  const strapiQuery = {
    populate: ["company", "company.logo", "company.coverImage", "skillsTags"],
    filters: {},
  };

  // boolean query filters
  if (query.remoteOkOnly) strapiQuery["filters"]["remoteOK"] = { $eq: true };

  if (query.featuredJobsOnly)
    strapiQuery["filters"]["featuredJob"] = { $eq: true };

  // range query filter
  strapiQuery["filters"]["baseAnnualSalary"] = {
    $gte: query.minBaseSalary,
    $lte: query.maxBaseSalary,
  };

  //inclusion query filters
  if (query.jobTypes && query.jobTypes.length) {
    strapiQuery["filters"]["jobTypes"] = { $in: query.jobTypes };
  }

  if (query.experienceLevels && query.experienceLevels.length) {
    strapiQuery["filters"]["experienceLevel"] = { $in: query.experienceLevels };
  }

  // nested inclusion query filters
  if (query.selectedTags && query.selectedTags.length)
    strapiQuery["filters"]["skillsTags"] = {
      name: { $in: query.selectedTags },
    };

  // full text search
  if (query.searchBarText) {
    const searchFields = [
      "title",
      "jobCategory",
      "jobTypes",
      "jobDescription",
      "aboutYou",
      "jobResponsibilities",
      "remunerationPackage",

      "skillsTags.name",
      "company.name",
      "company.city",
    ];

    strapiQuery["filters"]["$or"] = searchFields.map((field) => {
      const searchField = {};
      if (!field.includes(".")) {
        searchField[field] = { $containsi: query.searchBarText };
      } else {
        const [level1, level2] = field.split(".");
        const nestedSearchField = {};
        nestedSearchField[level2] = { $containsi: query.searchBarText };
        searchField[level1] = nestedSearchField;
      }
      return searchField;
    });
  }

  const strapiQueryStr = qs.stringify(strapiQuery, { encodeValuesOnly: true });

  const res = await axios.get(`${apiURL}/jobs?${strapiQueryStr}`);

  const rawJobs = res.data.data;

  const jobs = rawJobs.map((rawJob) => jobReducer(rawJob, false));

  return jobs;
};

export const getJobsSkills = async () => {
  const query = qs.stringify(
    {
      fields: ["name"],
      filters: {
        type: {
          $eq: "skill",
        },
      },
    },
    {
      encodeValuesOnly: true,
    }
  );
  const res = await axios.get(`${apiURL}/tags?${query}`);
  const rawTags = res.data.data;
  const tags = rawTags.map((rawTag) => {
    return rawTag.attributes.name;
  });
  return tags;
};
