import { PhoneIcon } from "@heroicons/react/solid";
import Image from "next/image";
import BannerImage from "../../public/images/banner.jpg";
import AvatarImage from "../../public/images/avatar.png";

const profile = {
  name: "RAKOTOBE Tiavina Mamihenintsoa",
  role: "Dev fullstack JS",
  companyURL: "https://tiavinarakotobe-v2.vercel.app",
  companyName: "tiavina",
  email: "mamihenintsoaRakotobe@gmail.com",
  message:
    "Hey there, If you ever need my services on a similar project, I'd love to help!",
  callToActionURL: "tel:+261346311091",
  callToActionMessage: "Book a Call With Me",
  profileImage: AvatarImage,
  coverImage: BannerImage,
};

export default function Header() {
  return (
    <div>
      <div className="mb-8">
        <div className="relative w-full h-60 lg:h-64">
          <Image
            className="object-cover scale-105"
            src={profile.coverImage}
            layout="fill"
            alt={`services offered by ${profile.companyName} - Headless Commerce & CMS Experts`}
          />
        </div>
        <div className="max-w-5xl px-4 mx-auto sm:px-6 lg:px-8">
          <div className="-mt-12 sm:-mt-16 sm:flex sm:items-end sm:space-x-5">
            <div className="flex">
              <div className="relative rounded-full h-28 w-28 ring-4 ring-slate-200 sm:h-28 sm:w-28">
                <Image
                  src={profile.profileImage}
                  layout="fill"
                  alt={`profile picture ${profile.name}`}
                />
              </div>
            </div>
            <div className="mt-4 sm:mt-12 sm:flex-1 sm:min-w-0 sm:flex sm:items-center sm:justify-end sm:space-x-6 sm:pb-1">
              <div className="flex-1 min-w-0 mt-6 sm:hidden md:block">
                <h1 className="text-2xl font-bold text-gray-900 truncate">
                  {profile.name}
                </h1>
                <p className="text-sm font-medium text-gray-500">
                  {profile.role} at{" "}
                  <a
                    href={profile.companyURL}
                    className="text-gray-900 hover:text-custompink-700"
                  >
                    {profile.companyName}
                  </a>{" "}
                </p>
                <p className="text-sm font-light text-custompink-600">
                  {profile.message}
                </p>
              </div>
              <div className="flex flex-col mt-4 space-y-3 justify-stretch sm:flex-row sm:space-y-0 sm:space-x-4">
                <a
                  href={profile.callToActionURL}
                  className="inline-flex items-center px-4 py-2 text-sm font-medium text-white border border-transparent rounded-md shadow-sm bg-custompink-800 hover:bg-custompink-900 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-custompink-500"
                >
                  <PhoneIcon
                    className="w-5 h-5 mr-2 -ml-1"
                    aria-hidden="true"
                  />
                  {profile.callToActionMessage}
                </a>
              </div>
            </div>
          </div>

          <div className="flex-1 hidden min-w-0 mt-6 sm:block md:hidden">
            <h1 className="text-2xl font-bold text-gray-900 truncate">
              {profile.name}
            </h1>
          </div>
        </div>
      </div>
    </div>
  );
}
