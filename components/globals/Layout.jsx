import Header from "./Header";

const Layout = ({ children }) => {
  return (
    <>
      {/* <Header /> */}
      <main className="p-2 m-2 mx-auto max-w-screen-2xl sm:px-6 lg:px-8 bg-slate-50">
        {children}
      </main>
    </>
  );
};

export default Layout;
