import React, { useState } from "react";
import { Switch } from "@headlessui/react";
import TagsFilterForm from "./TagsFilterForm";

function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}

function JobsPageSideBarForm({
  jobSkills,
  sideBarFormState,
  setSideBarFormState,
  setDisplayedJobs,
}) {
  const jobTypesOptions = [
    { value: "Full Time", display: "Full Time" },
    { value: "Part Time", display: "Part Time" },
    { value: "internship", display: "Internship" },
    { value: "Contract", display: "Contract" },
  ];

  const experienceLevelsOptions = [
    { value: "Junior", display: "Junior" },
    { value: "Medior", display: "Medior" },
    { value: "Senior", display: "Senior" },
    { value: "Tech Lead", display: "Tech Lead" },
  ];

  const baseSalaryRangesOptions = [
    { value: "<20K", display: "< £20K", bounds: { min: 0, max: 20000 } },
    {
      value: "20K-50K",
      display: "£20K - £50K",
      bounds: { min: 20001, max: 50000 },
    },
    {
      value: "50K-100K",
      display: "£50K - £100K",
      bounds: { min: 50001, max: 100000 },
    },
    {
      value: "> 100K",
      display: "> £100K",
      bounds: { min: 100001, max: 1000000 },
    },
  ];

  const handleRemoteOkChange = (checked) => {
    console.log(checked);
    //TODO: send request and filter jobs
    setSideBarFormState((prevState) => {
      return { ...prevState, remoteOkOnly: !prevState.remoteOkOnly };
    });
  };

  const handleFeaturedJobsOnlyChange = (checked) => {
    console.log(checked);
    //TODO: send request and filter jobs
    setSideBarFormState((prevState) => {
      return { ...prevState, featuredJobsOnly: !prevState.featuredJobsOnly };
    });
  };

  const handleJobTypeSelect = (e, option) => {
    console.log(e.target.checked, option);
    if (e.target.checked) {
      setSideBarFormState((prevState) => {
        const jobTypes = [...prevState.jobTypes];
        jobTypes.push(option);
        return { ...prevState, jobTypes };
      });
    } else {
      setSideBarFormState((prevState) => {
        return {
          ...prevState,
          jobTypes: prevState.jobTypes.filter((jobType) => option != jobType),
        };
      });
    }
  };

  const handleExperienceLevelsSelect = (e, option) => {
    console.log(e.target.checked, option);
    if (e.target.checked) {
      setSideBarFormState((prevState) => {
        const experienceLevels = [...prevState.experienceLevels];
        experienceLevels.push(option);
        return { ...prevState, experienceLevels };
      });
    } else {
      setSideBarFormState((prevState) => {
        return {
          ...prevState,
          experienceLevels: prevState.experienceLevels.filter(
            (experienceLevel) => option != experienceLevel
          ),
        };
      });
    }
  };

  const handleBaseSalaryRangesSelect = (e, option, bounds) => {
    console.log(e.target.checked, option, bounds);
    if (e.target.checked) {
      setSideBarFormState((prevState) => {
        const baseSalaryOptions = [...prevState.baseSalaryOptions];
        baseSalaryOptions.push(option);

        const baseSalaryBounds = [...prevState.baseSalaryBounds];
        baseSalaryBounds.push(bounds.min);
        baseSalaryBounds.push(bounds.max);

        const newFormState = {
          ...prevState,
          baseSalaryOptions,
          baseSalaryBounds,
        };
        console.log(newFormState);
        return newFormState;
      });
    } else {
      setSideBarFormState((prevState) => {
        const newFormState = {
          ...prevState,
          baseSalaryOptions: prevState.baseSalaryOptions.filter(
            (baseSalaryOption) => option != baseSalaryOption
          ),
          baseSalaryBounds: prevState.baseSalaryBounds.filter(
            (bound) => ![bounds.min, bounds.max].includes(bound)
          ),
        };

        console.log(newFormState);
        return newFormState;
      });
    }
  };

  return (
    <div className="space-y-8">
      {/* White box */}
      <div className="p-5 bg-white border rounded-sm shadow-lg border-slate-200">
        <div className="grid gap-6 md:grid-cols-2 xl:grid-cols-1">
          {/* Group 0*/}
          <TagsFilterForm
            jobSkills={jobSkills}
            selectedTags={sideBarFormState.selectedTags}
            setSideBarFormState={setSideBarFormState}
          />

          {/* Group 1 */}
          <Switch.Group as="div" className="flex items-center">
            <Switch
              checked={sideBarFormState.remoteOkOnly}
              onChange={handleRemoteOkChange}
              className={classNames(
                sideBarFormState.remoteOkOnly ? "bg-indigo-600" : "bg-gray-200",
                "relative inline-flex flex-shrink-0 h-6 w-11 border-2 border-transparent rounded-full cursor-pointer transition-colors ease-in-out duration-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
              )}
            >
              <span
                aria-hidden="true"
                className={classNames(
                  sideBarFormState.remoteOkOnly
                    ? "translate-x-5"
                    : "translate-x-0",
                  "pointer-events-none inline-block h-5 w-5 rounded-full bg-white shadow transform ring-0 transition ease-in-out duration-200"
                )}
              />
            </Switch>
            <Switch.Label as="span" className="ml-3">
              <span className="text-sm font-medium text-gray-900">
                Remote Ok Only
              </span>
            </Switch.Label>
          </Switch.Group>

          {/* Group 2 */}
          <Switch.Group as="div" className="flex items-center">
            <Switch
              checked={sideBarFormState.featuredJobsOnly}
              onChange={handleFeaturedJobsOnlyChange}
              className={classNames(
                sideBarFormState.featuredJobsOnly
                  ? "bg-indigo-600"
                  : "bg-gray-200",
                "relative inline-flex flex-shrink-0 h-6 w-11 border-2 border-transparent rounded-full cursor-pointer transition-colors ease-in-out duration-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
              )}
            >
              <span
                aria-hidden="true"
                className={classNames(
                  sideBarFormState.featuredJobsOnly
                    ? "translate-x-5"
                    : "translate-x-0",
                  "pointer-events-none inline-block h-5 w-5 rounded-full bg-white shadow transform ring-0 transition ease-in-out duration-200"
                )}
              />
            </Switch>
            <Switch.Label as="span" className="ml-3">
              <span className="text-sm font-medium text-gray-900">
                Featured Jobs Only
              </span>
            </Switch.Label>
          </Switch.Group>

          {/* Group 3 */}
          <div>
            <div className="mb-3 text-sm font-semibold text-slate-800">
              Job Types
            </div>
            <ul className="space-y-2">
              {jobTypesOptions.map((option) => {
                return (
                  <li key={option.value}>
                    <label className="flex items-center">
                      <input
                        type="checkbox"
                        className="form-checkbox"
                        onChange={(e) => handleJobTypeSelect(e, option.value)}
                        checked={sideBarFormState.jobTypes.includes(
                          option.value
                        )}
                      />
                      <span className="ml-2 text-sm font-medium text-slate-600">
                        {option.display}
                      </span>
                    </label>
                  </li>
                );
              })}
            </ul>
          </div>

          {/* Group 4 */}
          <div>
            <div className="mb-3 text-sm font-semibold text-slate-800">
              Experience Level
            </div>
            <ul className="space-y-2">
              {experienceLevelsOptions.map((option) => {
                return (
                  <li key={option.value}>
                    <label className="flex items-center">
                      <input
                        type="checkbox"
                        className="form-checkbox"
                        onChange={(e) =>
                          handleExperienceLevelsSelect(e, option.value)
                        }
                        checked={sideBarFormState.experienceLevels.includes(
                          option.value
                        )}
                      />
                      <span className="ml-2 text-sm font-medium text-slate-600">
                        {option.display}
                      </span>
                    </label>
                  </li>
                );
              })}
            </ul>
          </div>

          {/* Group 5 */}
          <div>
            <div className="mb-3 text-sm font-semibold text-slate-800">
              Salary Range
            </div>
            <ul className="space-y-2">
              {baseSalaryRangesOptions.map((option) => {
                return (
                  <li key={option.value}>
                    <label className="flex items-center">
                      <input
                        type="checkbox"
                        className="form-checkbox"
                        onChange={(e) =>
                          handleBaseSalaryRangesSelect(
                            e,
                            option.value,
                            option.bounds
                          )
                        }
                        checked={sideBarFormState.baseSalaryOptions.includes(
                          option.value
                        )}
                      />
                      <span className="ml-2 text-sm font-medium text-slate-600">
                        {option.display}
                      </span>
                    </label>
                  </li>
                );
              })}
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
}

export default JobsPageSideBarForm;
