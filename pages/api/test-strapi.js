import datasource from "../../datalayer";

export default async function handler(req, res) {
  // const data = await datasource.getCompanyBySlug({
  //   slug: "klanik-atypik-konsultants",
  // });
  const data = await datasource.getJobs();
  return res.status(200).json(data);
}
