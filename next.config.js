/** @type {import('next').NextConfig} */
const { withSuperjson } = require("next-superjson");

const nextConfig = {
  reactStrictMode: false,
  images: {
    domains: [process.env.STRAPI_IMAGES_DOMAIN],
  },
};

module.exports = withSuperjson()({
  ...nextConfig,
});
